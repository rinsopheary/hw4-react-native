import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from './components/SplashScreen'
import Login from './components/LoginScreen'
import Home from './components/Home';
import DetailFirstPage from './components/pages/DetailFirstPage';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Splash" 
        component={SplashScreen} 
        options = {{headerShown:null}}
        />
        <Stack.Screen 
        name="Login" 
        component={Login} 
        options = {{headerShown:null}} 
        />
        <Stack.Screen 
        name="Home" 
        component={Home} 
        options = {{headerShown:null}} 
        />

        <Stack.Screen 
        name="DetailFirstPage" 
        component={DetailFirstPage} 
        // options = {{headerShown:null}} 
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
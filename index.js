/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
// import LoginScreen from './components/LoginScreen';
// import LoginScreen from './components/LoginScreen';
// import Home from './components/Home'

AppRegistry.registerComponent(appName, () => App);

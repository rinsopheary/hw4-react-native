// React Native Tab
// https://aboutreact.com/react-native-tab/
import 'react-native-gesture-handler';

import * as React from 'react';
import {
  NavigationContainer
} from '@react-navigation/native';
import {
  createStackNavigator
} from '@react-navigation/stack';
import {
  createMaterialTopTabNavigator
} from '@react-navigation/material-top-tabs';
import TopicTab from './TopicTab'
import PeopleTab from './PeopleTab'
import PublicTab from './PublicTab';
/*import
  MaterialCommunityIcons
from 'react-native-vector-icons/MaterialCommunityIcons';*/


const Stack = createStackNavigator();
const Tab = createMaterialTopTabNavigator();

function InterestPage() {
  return (
    <Tab.Navigator
      initialRouteName="Feed"
      tabBarOptions={{
        activeTintColor: '#FFFFFF',
        inactiveTintColor: '#F8F8F8',
        style: {
          backgroundColor: 'green',
        },
        labelStyle: {
          textAlign: 'center',
        },
        indicatorStyle: {
          borderBottomColor: 'red',
          borderBottomWidth: 2,
        },
      }}>
      <Tab.Screen
        name="Topic"
        component={TopicTab}
        options={{
          tabBarLabel: 'Topic',
          // tabBarIcon: ({ color, size }) => (
          //   <MaterialCommunityIcons
          //       name="home"
          //       color={color}
          //       size={size}
          //     />
          // ),
        }}  />
      <Tab.Screen
        name="People"
        component={PeopleTab}
        options={{
          tabBarLabel: 'People',
          // tabBarIcon: ({ color, size }) => (
          //   <MaterialCommunityIcons
          //       name="settings"
          //       color={color}
          //       size={size}
          //     />
          // ),
        }} />

        <Tab.Screen
        name="Public"
        component={PublicTab}
        options={{
          tabBarLabel: 'PUBLICATIONS',
          // tabBarIcon: ({ color, size }) => (
          //   <MaterialCommunityIcons
          //       name="settings"
          //       color={color}
          //       size={size}
          //     />
          // ),
        }} />
    </Tab.Navigator>
  );
}
export default InterestPage;

// function InterestPage() {
//   return (
//     // <NavigationContainer>
//       <Stack.Navigator
//         initialRouteName="Settings"
//         screenOptions={{
//           headerStyle: { backgroundColor: '#633689' },
//           headerTintColor: '#fff',
//           headerTitleStyle: { fontWeight: 'bold' }
//         }}>
//         <Stack.Screen
//           // name="TabStack"
//           // component={TabStack}
//           options={{ title: 'Tab Stack' }}
//         />
//       </Stack.Navigator>
//     // </NavigationContainer>
//   );
// }


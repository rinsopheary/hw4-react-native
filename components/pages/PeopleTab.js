import { Card , Button} from 'native-base'
import React, { useState } from 'react'
import { Alert, StyleSheet, Text, TouchableOpacity, View, Image} from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import { set } from 'react-native-reanimated'

const PeopleTab = () => {
    const [data, setData] = useState([
        {
            key : 1,
            name: 'Lonaldo Bopha',
            position : 'President of US',
            sc : require('../assets/images/trum.jpg'),
            follow : 'Follow'
        },
        {
            key : 2,
            name: 'Jhon Dara',
            position : 'Marketing',
            sc : require('../assets/images/holy.jpg'),
            follow : 'Follow'
        },
        {
            key : 3,
            name: 'Lonaldo Bopha',
            position : 'President of US',
            sc : require('../assets/images/trum.jpg'),
            follow : 'Follow'
        },
        {
            key : 4,
            name: 'Jhon Dara',
            position : 'Marketing',
            sc : require('../assets/images/holy.jpg'),
            follow : 'Follow'
        },
        {
            key : 5,
            name: 'Lonaldo Bopha',
            position : 'President of US',
            sc : require('../assets/images/trum.jpg'),
            follow : 'Follow'
        },
        {
            key : 6,
            name: 'Jhon Dara',
            position : 'Marketing',
            sc : require('../assets/images/holy.jpg'),
            follow : 'Follow'
        },
    ])

    const handleClick = (index) =>{
        const arr = [...data]
        if (index + 1 === arr[index].key) {
            arr[index].follow === 'Follow'
              ? (arr[index].follow = 'Following')
              : (arr[index].follow = 'Follow');
          }
          setData(arr);
          console.log(arr);
          
    }

    const handlerDelete = (key)=>{
        setData((prevTodo) => {
            return prevTodo.filter(data => data.key != key)
        })
    }

    const {container, itemStyle, imageStyle} = styles
    return (
        <View style = {container}>
            <FlatList 
                data = {data}
                renderItem = {({item, index}) => (
                    <TouchableOpacity onLongPress={()=>handlerDelete(item.key)}>
                        {/* <View 
                        style={itemStyle}>
                        <Text>{item.story}</Text>
                        <Button 
                        onPress = {()=>handleClick(index)}
                        style={{padding : 20}}>
                            <Text>{item.follow}</Text>
                        </Button>
                    </View> */}
                    <View style={itemStyle}>
                        <View>
                            <Image 
                            source={item.sc}
                            style ={imageStyle}
                            />
                        </View>
                        <View>
                            <Text
                            numberOfLines={1} 
                            style={{fontSize: 17, fontWeight: "bold"}}>{item.name}</Text>
                            <Text 
                            numberOfLines={2}
                            style={{fontSize: 17, marginTop: 10}}>{item.position}</Text>
                        </View>
                        <View>
                            <Button 
                                onPress = {()=>handleClick(index)}
                                style={{padding: 20}}
                            >
                                <Text>{item.follow}</Text>
                            </Button>
                        </View>

                    </View>
                    </TouchableOpacity>
                )}
            />
        </View>
    )
}

export default PeopleTab

const styles = StyleSheet.create({
    container : {
        padding : 15
    },
    itemStyle : {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginBottom : 30
    },
    imageStyle : {
        width : 80,
        height :80,
        borderRadius : 80/2,
    }
})

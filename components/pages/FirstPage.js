// // Custom Navigation Drawer / Sidebar with Image and Icon in Menu Options
// // https://aboutreact.com/custom-navigation-drawer-sidebar-with-image-and-icon-in-menu-options/

// import * as React from 'react';
// import {Button, View, Text, SafeAreaView} from 'react-native';

// const FirstPage = ({navigation}) => {
//   return (
//     <SafeAreaView style={{flex: 1}}>
//       <View style={{flex: 1, padding: 16}}>
//         <View
//           style={{
//             flex: 1,
//             alignItems: 'center',
//             justifyContent: 'center',
//           }}>
//           <Text
//             style={{
//               fontSize: 25,
//               textAlign: 'center',
//               marginBottom: 16,
//             }}>
//             This is the First Page under First Page Option
//           </Text>
//           <Button
//             onPress={() => navigation.navigate('SecondPage')}
//             title="Go to Second Page"
//           />
//           <Button
//             onPress={() => navigation.navigate('ThirdPage')}
//             title="Go to Third Page"
//           />
//         </View>
//         <Text
//           style={{
//             fontSize: 18,
//             textAlign: 'center',
//             color: 'grey'
//           }}>
//           Custom React Navigate Drawer
//         </Text>
//         <Text
//           style={{
//             fontSize: 16,
//             textAlign: 'center',
//             color: 'grey'
//           }}>
//           www.aboutreact.com
//         </Text>
//       </View>
//     </SafeAreaView>
//   );
// };

// export default FirstPage;

import React, { useState } from 'react'
import { FlatList, StyleSheet, Text, View, Image, TouchableOpacity, ScrollView} from 'react-native'
import {Card} from 'native-base'

const FirstPage = ({navigation}) => {
  const [data, setData] = useState([
    {
      key : 1,
      title : 'Passing parameters to routes',
      author : 'Niki Bopha',
      created : '4 mns read',
      image : require('../assets/images/react2.png'),
      detail : 'Array is used to store multiple values in a single object in react native. Array can hold infinite values(According to your requirement) in a single variable. So in this tutorial we would going to Create and Show Array Elements in Text component using MAP function. So let’s get started 🙂 .'
    },
    {
      key : 2,
      title : 'Create native apps for Android and iOS using React',
      author : 'John Dara',
      created : '4 mns read',
      image : require('../assets/images/react.png'),
      detail : 'The move by the Office of Management and Budget meshes with other moves from the Trump campaign to deny that Biden will be president in 10 weeks and stall the transition as Trump fights the election in the courts.The Trump-appointed head of the General Services Administration has also declined to acknowledge Bidens victory, making it impossible for his transition team to get the funds needed to physically move office and to obtain access to sensitive government documents.'
    },
    {
      key : 3,
      title : 'Create native apps',
      author : 'Holy Pisey',
      created : '4 mns read',
      image : require('../assets/images/holy.jpg'),
      detail : 'Holy Moly (also spelled Holy Moley) is an exclamation of surprise that dates from at least 1892.[1] It is most likely a minced oath, a cleaned-up version of a taboo phrase such as "Holy Moses".[2]Holy Moly (also spelled Holy Moley) is an exclamation of surprise that dates from at least 1892.[1] It is most likely a minced oath, a cleaned-up version of a taboo phrase such as "Holy Moses".'
    },
    {
      key : 4,
      title : 'Passing parameters to routes',
      author : 'Niki Bopha',
      created : '4 mns read',
      image : require('../assets/images/trum.jpg'),
      detail : 'Array is used to store multiple values in a single object in react native. Array can hold infinite values(According to your requirement) in a single variable. So in this tutorial we would going to Create and Show Array Elements in Text component using MAP function. So let’s get started 🙂 .'
    },
    {
      key : 5,
      title : 'Create native apps for Android and iOS using React',
      author : 'John Dara',
      created : '4 mns read',
      image : require('../assets/images/react.png'),
      detail : 'The move by the Office of Management and Budget meshes with other moves from the Trump campaign to deny that Biden will be president in 10 weeks and stall the transition as Trump fights the election in the courts.The Trump-appointed head of the General Services Administration has also declined to acknowledge Bidens victory, making it impossible for his transition team to get the funds needed to physically move office and to obtain access to sensitive government documents.'
    },
    {
      key : 6,
      title : 'Create native apps',
      author : 'Holy Pisey',
      created : '4 mns read',
      image : require('../assets/images/holy.jpg'),
      detail : 'Holy Moly (also spelled Holy Moley) is an exclamation of surprise that dates from at least 1892.[1] It is most likely a minced oath, a cleaned-up version of a taboo phrase such as "Holy Moses".[2]Holy Moly (also spelled Holy Moley) is an exclamation of surprise that dates from at least 1892.[1] It is most likely a minced oath, a cleaned-up version of a taboo phrase such as "Holy Moses".'
    },
  ])
  const handleClick = (index) =>{
    // const selectedItem = data.indexOf(index);
    navigation.navigate('DetailFirstPage', {getIndex : data[index]})
  }
  return (
    <ScrollView>
      <View>
      <Text style={{fontSize: 25, fontWeight: "bold", padding: 15}}>Your Daily Read</Text>
      <FlatList 
      nestedScrollEnabled={true}
        data = {data}
        renderItem = {({item, index})=>(
          <TouchableOpacity
          onPress = {()=>handleClick(index)}
          >
            <Card>
            <View style={{flexDirection: "row", justifyContent: "space-between", padding: 15}}>
              <View style={{width: '70%'}}>
                <Text numberOfLines={2}
                 style={{fontSize: 20, fontWeight: "bold", marginBottom: 12}}
                >{item.title}</Text>
                <Text style={{fontSize: 15, marginBottom: 8}}>{item.author}</Text>
                <Text style={{color: 'grey'}}>{item.created}</Text>
              </View>
              <View style={{width: '30%'}}>
                <Image source={item.image} style={{width: 120, height: 120, resizeMode: "cover"}}/>
              </View>
            </View>
          </Card>
          </TouchableOpacity>
        )}
      />
    </View>
    </ScrollView>
  )
}

export default FirstPage

const styles = StyleSheet.create({})
